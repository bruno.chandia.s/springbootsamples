package cl.demo.web.demoweb.services;

import javax.xml.bind.JAXBElement;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import cl.demo.web.demoweb.services.clientBLZ.DetailsType;
import cl.demo.web.demoweb.services.clientBLZ.GetBankResponseType;
import cl.demo.web.demoweb.services.clientBLZ.GetBankType;
import cl.demo.web.demoweb.services.clientBLZ.ObjectFactory;

public class BankClient extends WebServiceGatewaySupport {

  public DetailsType getBank(String blz) {
      GetBankType request = new GetBankType();
      request.setBlz(blz);

      ObjectFactory oFactory = new ObjectFactory();
      JAXBElement<GetBankType> jaxEBank = oFactory.createGetBank(request);
      // JAXBElement<GetBankResponseType> jaxBankResponseType =  

      JAXBElement<GetBankResponseType> response = (JAXBElement<GetBankResponseType>) getWebServiceTemplate()
        .marshalSendAndReceive(jaxEBank);
        
      return response.getValue().getDetails();
  }
}