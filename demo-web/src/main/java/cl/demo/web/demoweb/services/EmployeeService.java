package cl.demo.web.demoweb.services;

import java.util.List;

import cl.demo.web.demoweb.model.Bank;
import cl.demo.web.demoweb.model.Employee;

public interface EmployeeService {
  List<Employee> getAllEmployees();
  Bank getBank(String blz);
}