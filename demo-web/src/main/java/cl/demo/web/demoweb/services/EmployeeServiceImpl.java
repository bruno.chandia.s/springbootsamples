package cl.demo.web.demoweb.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.demo.web.demoweb.model.Bank;
import cl.demo.web.demoweb.model.Employee;
import cl.demo.web.demoweb.services.clientBLZ.DetailsType;

@Service
public class EmployeeServiceImpl implements EmployeeService {

  @Autowired
  private BankClient bankClient;

  @Override
  public List<Employee> getAllEmployees() {
    ArrayList<Employee> employees = new ArrayList<Employee>();
    employees.add(new Employee(1, "Juan Pérez", "14588688-0"));
    employees.add(new Employee(2, "Jose martínez", "21882408-0"));
    employees.add(new Employee(3, "Juan Alfonso Lagos", "13088211-0"));
    return employees;
  }

  @Override
  public Bank getBank(String blz) {
    DetailsType details = bankClient.getBank(blz);
    return new Bank(details.getBezeichnung(), details.getBic(), details.getOrt(), details.getPlz());
  }
}