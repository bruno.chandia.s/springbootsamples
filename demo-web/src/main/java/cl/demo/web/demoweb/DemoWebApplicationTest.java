package cl.demo.web.demoweb;

import org.junit.jupiter.api.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import cl.demo.web.demoweb.controller.EmployeeController;

import static org.assertj.core.api.Assertions.assertThat; 

@SpringBootTest
public class DemoWebApplicationTest {

  @Autowired
  private EmployeeController controller;

  @Test
  public void contextLoads() throws Exception {
    assertThat(controller).isNull();
  }

}
