package cl.demo.web.demoweb.model;

import java.util.Objects;

public class Employee {
  long id;
  String name;
  String rut;


  public Employee() {
  }

  public Employee(long id, String name, String rut) {
    this.id = id;
    this.name = name;
    this.rut = rut;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getRut() {
    return this.rut;
  }

  public void setRut(String rut) {
    this.rut = rut;
  }

  public Employee id(long id) {
    setId(id);
    return this;
  }

  public Employee name(String name) {
    setName(name);
    return this;
  }

  public Employee rut(String rut) {
    setRut(rut);
    return this;
  }

  @Override
  public boolean equals(Object o) {
      if (o == this)
          return true;
      if (!(o instanceof Employee)) {
          return false;
      }
      Employee employee = (Employee) o;
      return id == employee.id && Objects.equals(name, employee.name) && Objects.equals(rut, employee.rut);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, rut);
  }

  @Override
  public String toString() {
    return "{" +
      " id='" + getId() + "'" +
      ", name='" + getName() + "'" +
      ", rut='" + getRut() + "'" +
      "}";
  }

}