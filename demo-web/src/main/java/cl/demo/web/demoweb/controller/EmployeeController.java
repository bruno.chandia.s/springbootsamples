package cl.demo.web.demoweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import cl.demo.web.demoweb.services.EmployeeService;

@Controller
public class EmployeeController {

  @Autowired
  private EmployeeService employeeService;

  @GetMapping("/") 
  public String viewHomePage (Model model) {
    model.addAttribute("listEmployees", employeeService.getAllEmployees());
    model.addAttribute("bank", employeeService.getBank("10070024"));
    return "index";
  }
}