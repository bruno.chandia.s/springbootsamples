package cl.demo.web.demoweb.services;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class BanksClientConfig {

  @Bean
  public Jaxb2Marshaller marshaller() {
    Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
    marshaller.setContextPath("cl.demo.web.demoweb.services.clientBLZ");
    return marshaller;
  }

  @Bean
  public BankClient bankClient(Jaxb2Marshaller marshaller) {
    BankClient client = new BankClient();
    client.setDefaultUri("http://www.thomas-bayer.com/axis2/services/BLZService");
    client.setMarshaller(marshaller);
    client.setUnmarshaller(marshaller);
    return client;
  }

}