package cl.demo.web.demoweb.model;

public class Bank {

  private String bezeichnung;
  private String bic;
  private String ort;
  private String plz;
  
  public Bank(String bezeichnung, String bic, String ort, String plz) {
    this.bezeichnung = bezeichnung;
    this.bic = bic;
    this.ort = ort;
    this.plz = plz;
  }

  public String getBezeichnung() {
    return bezeichnung;
  }

  public void setBezeichnung(String bezeichnung) {
    this.bezeichnung = bezeichnung;
  }

  public String getBic() {
    return bic;
  }

  public void setBic(String bic) {
    this.bic = bic;
  }

  public String getOrt() {
    return ort;
  }

  public void setOrt(String ort) {
    this.ort = ort;
  }

  public String getPlz() {
    return plz;
  }

  public void setPlz(String plz) {
    this.plz = plz;
  }

  @Override
  public String toString() {
    return "Bank [bezeichnung=" + bezeichnung + ", bic=" + bic + ", ort=" + ort + ", plz=" + plz + "]";
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Bank other = (Bank) obj;
    if (bezeichnung == null) {
      if (other.bezeichnung != null)
        return false;
    } else if (!bezeichnung.equals(other.bezeichnung))
      return false;
    if (bic == null) {
      if (other.bic != null)
        return false;
    } else if (!bic.equals(other.bic))
      return false;
    if (ort == null) {
      if (other.ort != null)
        return false;
    } else if (!ort.equals(other.ort))
      return false;
    if (plz == null) {
      if (other.plz != null)
        return false;
    } else if (!plz.equals(other.plz))
      return false;
    return true;
  }

  
  
}
